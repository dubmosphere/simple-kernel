#!/bin/bash

BUILD_DIR=bin
ISO_DIR=iso
ISO_LABEL="Simple Kernel"

# Create directory the iso is generated from
mkdir $ISO_DIR

# Copy binary/binaries to iso folder
cp $BUILD_DIR/os_image.bin $ISO_DIR/

# Generate ISO image from iso folder
genisoimage -quiet -V "$ISO_LABEL" \
    -o $BUILD_DIR/os_image.iso -b os_image.bin $ISO_DIR/

# Remove iso folder from disk, we now have the iso
rm -r $ISO_DIR

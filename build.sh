#!/bin/bash

INCLUDE_DIR=include
SRC_DIR=src
OBJ_DIR=obj
BUILD_DIR=bin
ASM_DIR=asm
SIMIX_DIR=simix

# Create used directories if needed
if [ ! -d $BUILD_DIR ]; then
    mkdir $BUILD_DIR
fi

if [ ! -d $OBJ_DIR ]; then
    mkdir $OBJ_DIR
fi

if [ ! -d $OBJ_DIR/$SIMIX_DIR ]; then
    mkdir $OBJ_DIR/$SIMIX_DIR
fi

# Build boot sector
nasm -f bin -i $INCLUDE_DIR/$ASM_DIR/ -o $BUILD_DIR/boot.bin $SRC_DIR/$ASM_DIR/boot.asm
nasm -f elf -o $OBJ_DIR/kernel_entry.o $SRC_DIR/$ASM_DIR/kernel_entry.asm

# Build kernel
for f in $(find $SRC_DIR -name '*.cpp'); do
    OBJ_FILE=$OBJ_DIR/${f//$SRC_DIR\//}
    OBJ_FILE=${OBJ_FILE/.cpp/.o}
    if [ -f simplekernel.config ]; then
        g++ --std=c++14 -O3 -ffreestanding -includesimplekernel.config -I$INCLUDE_DIR -o $OBJ_FILE -c $f -m32
    else
        g++ --std=c++14 -O3 -ffreestanding -I$INCLUDE_DIR -o $OBJ_FILE -c $f -m32
    fi
    LINKER_STR="$LINKER_STR $OBJ_FILE"
done

# Link kernel
g++ -Ttext 0x7e00 \
    -e main \
    -o $OBJ_DIR/kernel.elf \
    $OBJ_DIR/kernel_entry.o $LINKER_STR \
    -nostdlib \
    -lgcc \
    -m32

objcopy -O binary $OBJ_DIR/kernel.elf $BUILD_DIR/kernel.bin             # Copy the binary to a raw binary format

# Concatenate image out of boot sector and kernel and truncate it for qemu
cat $BUILD_DIR/boot.bin $BUILD_DIR/kernel.bin > $BUILD_DIR/os_image.bin

# Truncate image to be allowed size
truncate $BUILD_DIR/os_image.bin -s 2880k

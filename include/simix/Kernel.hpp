#ifndef SIMIX_KERNEL_HPP
#define SIMIX_KERNEL_HPP

namespace simix {

class Kernel {
    public:
        int run();
};

}

#endif // SIMIX_KERNEL_HPP

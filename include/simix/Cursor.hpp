#ifndef SIMIX_CURSOR_HPP
#define SIMIX_CURSOR_HPP

namespace simix {

class Cursor {
    public:
        Cursor(char character, unsigned char color);

        char getChar();
        unsigned char getColor();

        int x;
        int y;

    private:
        char character;
        unsigned char color;
};

}

#endif // SIMIX_CURSOR_HPP

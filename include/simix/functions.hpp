#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#define VIDEO_MEMORY 0xb8000
#define RESOLUTION 80 * 25
#define DEFAULT_COLOR 0x07

void putc_offset(char c, int offset, unsigned char color = DEFAULT_COLOR);
void putc(char c, int x = 0, int y = 0, unsigned char color = DEFAULT_COLOR);
void print_offset(const char *message, int offset = 0, unsigned char color = DEFAULT_COLOR);
void print(const char *message, int x = 0, int y = 0, unsigned char color = DEFAULT_COLOR);
void cls(unsigned char color = DEFAULT_COLOR);

unsigned int strlen(const char *str);
void strcpy(const char *str, char *copy, int size = 0);
void strrev(char *str, int size = 0);
bool strcmp(const char *str1, const char *str2);
char *itoa(int value, char *str, int base);

unsigned char inb(unsigned short port);
void outb(unsigned short port, unsigned char value);

#endif // FUNCTIONS_HPP

#ifndef SIMIX_CONSOLE_HPP
#define SIMIX_CONSOLE_HPP

#include "simix/Cursor.hpp"

namespace simix {

class Console {
    public:
        Console();

        const Cursor &getCursor();

    private:
        Cursor cursor;
};

}

#endif // SIMIX_CONSOLE_HPP

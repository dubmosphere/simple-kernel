[BITS 32]

;pm_putc:
;    pusha
;
;    mov edx, VIDEO_MEMORY
;    add edx, ebx
;    mov [edx], ax

;    popa
;    ret

;pm_print:
;    pusha
;    mov edx, VIDEO_MEMORY   ; Set edx to start of video memory

;    .print_pm_loop:
;        mov al, [esi]           ; Set al to address esi
;        mov ah, WHITE_ON_BLACK  ; Set color to white on black

;        cmp al, 0               ; Compare al with 0
;        je .print_pm_return     ; If al is zero jump to return

;        mov [edx], ax           ; Move ax into video memory edx
;        inc esi                 ; Increment esi
;        add edx, 2              ; Increment edx by two

;        jmp .print_pm_loop

;    .print_pm_return:
;        popa
;    ret

;pm_cls:
;    pusha
;    mov edx, VIDEO_MEMORY
;    mov al, 0
;    mov ah, WHITE_ON_BLACK
;    mov ecx, 0

;    .pm_cls_loop:
;        cmp ecx, RESOLUTION
;	je .pm_cls_return

;        mov [edx], ax
;	inc ecx
;	add edx, 2

;        jmp .pm_cls_loop

;    .pm_cls_return:
;        popa
;    ret

; Constants
;VIDEO_MEMORY equ 0xb8000
;WHITE_ON_BLACK equ 0x0f
;RESOLUTION equ 80 * 25

[BITS 16]

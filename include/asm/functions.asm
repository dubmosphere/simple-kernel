; Print a string
print:
    push ax
    push bx
    mov ah, 0x0E    ; We are using the teletype function
    mov bh, 0x00    ; Page 0
    mov bl, 0x07    ; Color to black background and green font if possible

    .print_nextchar:
        lodsb                   ; Load the current char from address SI into AL and increment si to the next address

        or al, al		; Check if we reached the 0 terminator
        jz .print_return	; If we reached the 0 terminator, return

        int 0x10		; Run display interrupt
        jmp .print_nextchar	; Continue with the next char

        .print_return:
            pop bx
            pop ax
        ret

; Prints a hexadecimal number to screen
printhex:
    push bx
    push si

    mov bx, dx                  ; Set bx to dx
    shr bx, 12                  ; Shift bx by 12 bits so the X??? -> 000X
    mov bx, [bx+HEXABET]        ; Set bx to the according character in the HEXABET
    mov [HEX_TEMPLATE+2], bl    ; Swap character at position 2 in HEX_TEMPLATE with bl

    mov bx, dx                  ; Set bx to dx
    shr bx, 8                   ; Shift bx by 8 bits so the XX?? -> 00XX
    and bx, 0x000f              ; And the shifted bits with 0x000f to get 000X
    mov bx, [bx+HEXABET]        ; Look up the HEXABET
    mov [HEX_TEMPLATE+3], bl    ; Set the value in the HEX_TEMPLATE

    mov bx, dx                  ; Set bx to dx
    shr bx, 4                   ; Shift bx by 4 bits so the XXX? -> 0XXX
    and bx, 0x000f              ; And the shifted bits with 0x000f to get 000X
    mov bx, [bx+HEXABET]        ; Look up the HEXABET
    mov [HEX_TEMPLATE+4], bl    ; Set the value in the HEX_TEMPLATE

    mov bx, dx                  ; Set bx to dx
    and bx, 0x000f              ; And the of bx bits with 0x000f to get 000X
    mov bx, [bx+HEXABET]        ; Look up the HEXABET
    mov [HEX_TEMPLATE+5], bl    ; Set the value in the HEX_TEMPLATE

    mov si, HEX_TEMPLATE        ; Move the address of HEX_TEMPLATE into si
    call print                  ; Print the hex value as ASCII

    pop si
    pop bx
    ret

; Reads al sectors after boot sector from disk
read_from_disk:
    pusha

    mov ah, 0x02    ; Function: Read sectors from drive

;    mov al, 1       ; Number of sectors to read
    mov ch, 0       ; Select first cylinder/track
    mov dh, 0       ; Select first read/write head
;    mov cl, 2       ; Select second sector

    push bx
    mov bx, 0
    mov es, bx          ; Setting extra segment to 0
    pop bx
;    mov bx, start + 512 ; Memory location to load the sector in

    int 0x13            ; Disk I/O-Interrupt
    jc .read_error      ; Interrupt sets CF if an error occurs so we can handle it
    jmp .read_return    ; Else if no CF is set return

    .read_error:
        push dx
        mov dx, ax          ; Move ax into dx
        call printhex       ; Print the return code and read content size
        pop dx
        mov si, DISK_ERROR  ; Move address of DISK_ERROR into si
        call print          ; Print DISK_ERROR
        jmp $               ; Hang CPU

    .read_return:
        popa
    ret

; Hides the HW cursor
hide_cursor:
    push ax
    push cx
    mov cx, 0x2000
    mov ah, 1
    int 0x10
    pop cx
    pop ax
    ret

; Data
HEX_TEMPLATE db '0x???? ', 0
HEXABET db '0123456789ABCDEF', 0
DISK_ERROR db "Error reading disk!", 13, 10, 0

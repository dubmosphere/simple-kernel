gdt_start:
    gdt_null:   ; Mandatory null descriptor
        dd 0x0
        dd 0x0

    ; 1st flags: 1 (present) 00 (privilege) 1 (descriptor type) -> 1001b
    ; type flags 1 (code) 0 (conforming) 1 (readable) 0 (accessed) -> 1010b
    ; 2nd flags: 1 (granularity) 1 (32-bit default) 0 (64-bit segment) 0 (AVL) -> 1100b
    gdt_code:
        dw 0xffff       ; Declare limit low (bits 0-15)
        dw 0x0          ; Declare base low (bits 0-15)
        db 0x0          ; Declare base middle (bits 16-23)
        db 0x9a         ; 1st flags: Access
        db 0xcf         ; 2nd flags: Granularity and Limit high (bits 16-19)
        db 0x0          ; Declare base high (bits 24-31)

    ; type flags 0 (code) 0 (expand down) 1 (writable) 0 (accessed) -> 0010b
    gdt_data:
        dw 0xffff       ; Declare limit low (bits 0-15)
        dw 0x0          ; Declare base low (bits 0-15)
        db 0x0          ; Declare base middle (bits 16-23)
        db 0x92         ; 1st flags: Access
        db 0xcf         ; 2nd flags: Granularity and Limit high (bits 16-19)
        db 0x0          ; Declare base high (bits 24-31)

    gdt_descriptor:
        dw $ - gdt_start - 1
        dd gdt_start

; Data
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

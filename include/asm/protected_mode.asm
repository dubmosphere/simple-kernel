; Include our global descriptor table
%include "gdt.asm"

; Switch to protected mode
switch_to_pm:
    cli                     ; Switch off interrupts until we have set up the protected mode interrupt vector
    lgdt [gdt_descriptor]   ; Load our global descriptor table (Defines protected mode segments)

    push eax
    mov eax, cr0            ; Set the first bit of cr0 to switch to pm, so we have to load cr0 into eax
    or eax, 0x1             ; Set the first bit
    mov cr0, eax            ; Set cr0 to eax again
    pop eax

    jmp CODE_SEG:pm_init

[BITS 32]

; Initialize protected mode
pm_init:
    push ax
    mov ax, DATA_SEG    ; Segments become meaningless,
    mov ds, ax          ; so we point the segment registers to the data selector defined in the gdt
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    pop ax

    mov ebp, 0x90000    ; Update the base pointer
    mov esp, ebp        ; Update stack pointer

    call BEGIN_PM       ; Begin the protected mode
    ret

[BITS 16]

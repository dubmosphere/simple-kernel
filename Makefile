.PHONY: all bin iso clean

all: bin iso

bin:
	bash build.sh

iso:
	bash geniso.sh

clean:
	bash clean.sh

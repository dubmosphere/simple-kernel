[BITS 16]
[ORG 0x7c00]

_start:
    mov ax, 0x0000      ; Set ax to data segment address
    mov ds, ax          ; Set data segment
    mov bp, 0xffff      ; Set base pointer
    mov sp, bp          ; Set stack pointer

    push dx

    mov al, 18              ; Read 18 sectors from disk
    mov cl, 2               ; Select second sector to start reading from
    mov bx, KERNEL_OFFSET   ; Read from disk to kernel_entry address (0x7e00)
    call read_from_disk     ; Read from disk

    call hide_cursor        ; Hide the HW cursor

    call switch_to_pm       ; Switch to protected mode

    pop dx

    jmp $   ; Hang CPU if we ever get here

[BITS 32]

BEGIN_PM:
    call KERNEL_OFFSET

    jmp $   ; Hang CPU if we ever get here
    ret

[BITS 16]

; Includes
%include "functions.asm"
%include "protected_mode.asm"
%include "pm_functions.asm"

; Data (Addresses are assembled away)
KERNEL_OFFSET equ 0x7e00

; Boot sector end
times 510 - ($ - $$) db 0       ; Fill the rest of bytes with 0s to fit the 512 bytes for the boot sector (Happens while assembling)
dw 0xaa55                       ; End of bootsector

#include "simix/functions.hpp"
#include "simix/Kernel.hpp"

using namespace simix;

int main() {
    Kernel kernel;

    return kernel.run();   // Return error state 0 (If the system returns 0 we have a really really strange error)
}

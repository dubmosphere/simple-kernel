#include "simix/functions.hpp"

void putc_offset(char c, int offset, unsigned char color) {
    unsigned short *videoMemory = reinterpret_cast<unsigned short*>(VIDEO_MEMORY) + offset;
    *videoMemory = static_cast<unsigned short>(c) | static_cast<unsigned short>(color) << 8;
}

// Print a char at x and y position
void putc(char c, int x, int y, unsigned char color) {
    putc_offset(c, y * 80 + x, color);
}


void print_offset(const char *message, int offset, unsigned char color) {
    const char *nextChar = message;

    for(unsigned int i = 0; *nextChar; i++) {
        putc_offset(*nextChar++, offset + i, color);
    }
}

// Print a message
void print(const char *message, int x, int y, unsigned char color) {
    print_offset(message, y * 80 + x, color);
}

// Clear the screen 
void cls(unsigned char color) {
    for(unsigned int i = 0; i < RESOLUTION; i++) {
        putc_offset(0, i, color);
    }
}

// Get the length of a string
unsigned int strlen(const char *str) {
    const char *nextChar = str;

    unsigned int len = 0;
    while(*nextChar++) len++;

    return len;
}

// Copy a string
void strcpy(const char *str, char *copy, int size) {
    if(!size) {
        size = strlen(str);
    }

    for(unsigned int i = 0; i < size; i++) {
        copy[i] = str[i];
    }
}

// Reverse a string
void strrev(char *str, int size) {
    if(!size) {
        size = strlen(str);
    }

    char temp[size];
    strcpy(str, temp, size);

    unsigned int j = 0;
    for(unsigned int i = size - 1; i >= 0; i--) {
        str[j++] = temp[i];
    }
}

// Compare two strings
bool strcmp(const char* str1, const char *str2) {
    unsigned int len = strlen(str1);

    if(len != strlen(str2)) {
        return false;
    }

    for(unsigned int i = 0; i < len; i++) {
        if(str1[i] != str2[i]) {
            return false;
        }
    }

    return true;
}

// Convert a number from a system to a string (In my opinion unnecessary complicated, it is copied from osdev, will write my own...)
char *itoa(int value, char *str, int base) {
    char *rc;
    char *ptr;
    char *low;

    if (base < 2 || base > 36) {
        *str = '\0';
        return str;
    }

    rc = ptr = str;

    if (value < 0 && base == 10) {
        *ptr++ = '-';
    }

    low = ptr;

    do {
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35 + value % base];
        value /= base;
    } while(value);

    *ptr-- = '\0';

    while (low < ptr) {
        char tmp = *low;
        *low++ = *ptr;
        *ptr-- = tmp;
    }

    return rc;
}

// Input 
unsigned char inb(unsigned short port) {
    unsigned char ret;

    asm volatile (
                "inb %1, %0"
                : "=a" (ret)
                : "Nd" (port)
                );

    return ret;
}

// Output
void outb(unsigned short port, unsigned char value) {
    asm volatile (
                "outb %0, %1"
                :
                : "a" (value), "Nd" (port)
                );
}

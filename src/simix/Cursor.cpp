#include "simix/Cursor.hpp"

namespace simix {

Cursor::Cursor(char character, unsigned char color) :
x {0},
y {0},
character {character},
color {color}
{

}

char Cursor::getChar()
{
    return character;
}

unsigned char Cursor::getColor()
{
    return color;
}

}

#include "simix/Kernel.hpp"

#include "simix/functions.hpp"
#include "simix/Console.hpp"

char scancodes[0xff] = {0};

void initScancodes() {
    scancodes[0x01] = 27;
    scancodes[0x02] = '1';
    scancodes[0x03] = '2';
    scancodes[0x04] = '3';
    scancodes[0x05] = '4';
    scancodes[0x06] = '5';
    scancodes[0x07] = '6';
    scancodes[0x08] = '7';
    scancodes[0x09] = '8';
    scancodes[0x0a] = '9';
    scancodes[0x0b] = '0';
    scancodes[0x10] = 'q';
    scancodes[0x11] = 'w';
    scancodes[0x12] = 'e';
    scancodes[0x13] = 'r';
    scancodes[0x14] = 't';
    scancodes[0x15] = 'z';
    scancodes[0x16] = 'u';
    scancodes[0x17] = 'i';
    scancodes[0x18] = 'o';
    scancodes[0x19] = 'p';
    scancodes[0x1a] = 'u';
    scancodes[0x1b] = '+';
    scancodes[0x1c] = '\n';
    scancodes[0x1e] = 'a';
    scancodes[0x1f] = 's';
    scancodes[0x20] = 'd';
    scancodes[0x21] = 'f';
    scancodes[0x22] = 'g';
    scancodes[0x23] = 'h';
    scancodes[0x24] = 'j';
    scancodes[0x25] = 'k';
    scancodes[0x26] = 'l';
    scancodes[0x27] = 'o';
    scancodes[0x28] = 'a';
    scancodes[0x2b] = '#';
    scancodes[0x2c] = 'y';
    scancodes[0x2d] = 'x';
    scancodes[0x2e] = 'c';
    scancodes[0x2f] = 'v';
    scancodes[0x30] = 'b';
    scancodes[0x31] = 'n';
    scancodes[0x32] = 'm';
    scancodes[0x33] = ',';
    scancodes[0x34] = '.';
    scancodes[0x35] = '-';
    scancodes[0x37] = '*';
    scancodes[0x39] = ' ';
    scancodes[0x47] = '7';
    scancodes[0x48] = '8';
    scancodes[0x49] = '9';
    scancodes[0x4a] = '-';
    scancodes[0x4b] = '4';
    scancodes[0x4c] = '5';
    scancodes[0x4d] = '6';
    scancodes[0x4e] = '+';
    scancodes[0x4f] = '1';
    scancodes[0x50] = '2';
    scancodes[0x51] = '3';
    scancodes[0x52] = '0';
    scancodes[0x53] = ',';
    scancodes[0x56] = '<';
}

namespace simix {

int Kernel::run() {
    cls();
    initScancodes();

    //Console console;
    //const Cursor &cursor = console.getCursor();

    //asm volatile ("sti");
    //asm volatile ("cli");

    // Debug output
    print("Debug output:");

    char scancode = 0;
    int x = 0;
    int y = 1;

    do {
        if(inb(0x60) != scancode) {
            scancode = inb(0x60);
            unsigned int index = static_cast<unsigned int>(scancode);

            if(scancode > 0) {
                if(x >= 80 || scancodes[index] == '\n') {
                    x = 0;
                    y++;
                }

                if(y >= 25) {
                    cls();
                    y = 0;
                }

                if(scancodes[index] == 27) {
                    break;
                }
                
                if(scancodes[index] != '\n') {
                    putc(scancodes[index], x++, y);
                }
//                char *debugAsciiCode;
//                debugAsciiCode = itoa(scancodes[scancode], debugAsciiCode, 10);
//                print(debugAsciiCode, x, y++);

//                char *debugScanCode;
//                debugScanCode = itoa(scancode, debugScanCode, 16);
//                print(debugScanCode, x, y++);
            }
        }
    } while(1);

    // Say hello
    print("HELLO WORLD!!!");

    // Say goodbye
    print("BYE WORLD!!!", 0, 24, 0x14);

    while(1);   // Hang CPU

    return 0;
}

}

#include "simix/Console.hpp"

namespace simix {

Console::Console() :
cursor(' ', 0x77)
{
}

const Cursor &Console::getCursor()
{
    return cursor;
}

}
